"""
 ******************************************************************************
 * Project: DL1_framework                                                     *
 * Package: atlas-flavor-tagging-tools                                        *
 *                                                                            *
 * Description:                                                               *
 *      Training script for DL1 tagger                                        *
 *                                                                            *
 * Authors:                                                                   *
 *      ATLAS flavour tagging group, CERN, Geneva                             *
 *                                                                            *
 ******************************************************************************
"""
from __future__ import print_function
import h5py
import numpy as np
import os
os.environ['KERAS_BACKEND'] = 'tensorflow'
# os.environ['PYTHONHASHSEED'] = '0'
# os.environ['OMP_NUM_THREADS'] = '1'

import argparse

# import necessary keras modules for training
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Conv1D, Conv2D, Conv3D, SimpleRNN, GlobalMaxPool1D
from keras.optimizers import Adam
from keras.layers.normalization import BatchNormalization

from hyperopt import Trials, STATUS_OK, tpe
from hyperas import optim
from hyperas.distributions import choice, uniform


# def get_args():
#     """Define arg parse arguments."""
#     parser = argparse.ArgumentParser(description=' Options for keras training')
#     parser.add_argument('-t', '--training_file', type=str,
#                         default='',
#                         help='Enter the name of preprocessed training file',
#                         required=True)
#     parser.add_argument('-v', '--validation_file', type=str,
#                         default='',
#                         help='Enter the name of preprocessed validation file',
#                         required=True)
#     parser.add_argument('-m', '--model_name', type=str,
#                         default='',
#                         help='''Enter Model name. Will create new folder in
#                         results where output files are stored. If not
#                         specified, results are directly stored in results
#                         folder.''')
#     return parser.parse_args()
#
#
# args = get_args()
# if not os.path.exists('results'):
#     os.makedirs('results')
# if args.model_name != '':
#     if not os.path.exists('results/%s' % args.model_name):
#         os.makedirs('results/%s' % args.model_name)


# def data():
#     """Retrieves the data and returns data to pass to the model."""
#     training_file = '/data/mg294/temp_tuples/hybrid_training_sample_MC16D_reduced_even_renamed_preprocessed_ordered.h5'
#     testinf = h5py.File(training_file, 'r')
#     train_bjet = testinf['train_processed_bjets'][:]
#     train_cjet = testinf['train_processed_cjets'][:]
#     train_ujet = testinf['train_processed_ujets'][:]
#     b_weight = train_bjet['weight']
#     c_weight = train_cjet['weight']
#     u_weight = train_ujet['weight']
#
#
#     var_names = list(train_bjet.dtype.names)
#     var_names.remove('label')
#     var_names.remove('weight')
#     X_train = np.concatenate((train_ujet, train_cjet, train_bjet))
#     y_train = np.concatenate((np.zeros(len(train_ujet)),
#                               np.ones(len(train_cjet)),
#                               2 * np.ones(len(train_bjet))))
#     Y_train = np_utils.to_categorical(y_train, 3)
#
#     X_train = X_train[var_names]
#     X_train = np.array(X_train.tolist())
#     X_weights = np.concatenate((u_weight, c_weight, b_weight))
#
#     rng_state = np.random.get_state()
#     np.random.shuffle(X_train)
#     np.random.set_state(rng_state)
#     np.random.shuffle(Y_train)
#     np.random.set_state(rng_state)
#     np.random.shuffle(X_weights)
#
#     print(np.shape(X_train))
#     print(np.shape(Y_train))
#     print(np.shape(X_weights))
#
#     return X_train, Y_train, X_weights
#     # return X_train, Y_train, X_test, Y_test


def GetData(f_name, weight=False):
    """Retrieves the data and provides them as vectors"""
    f = h5py.File(f_name, 'r')
    bjet = f['train_processed_bjets'][:]
    cjet = f['train_processed_cjets'][:]
    ujet = f['train_processed_ujets'][:]
    if weight:
        b_weight = bjet['weight']
        c_weight = cjet['weight']
        u_weight = ujet['weight']

    var_names = list(bjet.dtype.names)
    var_names.remove('label')
    var_names.remove('weight')
    X = np.concatenate((ujet, cjet, bjet))
    y = np.concatenate((np.zeros(len(ujet)), np.ones(len(cjet)),
                        2 * np.ones(len(bjet))))
    Y = np_utils.to_categorical(y, 3)
    X = X[var_names]
    X = np.array(X.tolist())

    rng_state = np.random.get_state()
    np.random.shuffle(X)
    np.random.set_state(rng_state)
    np.random.shuffle(Y)
    if weight:
        X_weights = np.concatenate((u_weight, c_weight, b_weight))
        np.random.set_state(rng_state)
        np.random.shuffle(X_weights)

    print(np.shape(X))
    print(np.shape(Y))
    if weight:
        print(np.shape(X_weights))

    if weight:
        return X, Y, X_weights
    else:
        return X, Y


def data():
    """Retrieves the data and returns data to pass to the model."""
    training_file = '/data/mg294/temp_tuples/hybrid_training_sample_MC16D_reduced_even_renamed_preprocessed_ordered.h5'
    test_file = '/data/mg294/temp_tuples/DL1validation_ttbar_MC16D_reduced_odd_renamed_preprocessed.h5'
    training_file = '/home/manuel/workspace/DL1_framework/samples/hybrid_training_sample_MC16D_reduced_even_renamed_preprocessed_ordered.h5'
    test_file = '/home/manuel/workspace/DL1_framework/samples/DL1validation_ttbar_MC16D_reduced_odd_renamed_preprocessed.h5'

    # reduced files
    training_file = '/home/manuel/workspace/DL1_framework/samples/hybrid_training_renamed_preprocessed_ordered_reduced.h5'
    valid_file = '/home/manuel/workspace/DL1_framework/samples/ttbar_MC16D_reduced_valid.h5'
    test_file = '/home/manuel/workspace/DL1_framework/samples/ttbar_MC16D_reduced_test.h5'

    # from sklearn.manifold import TSNE
    from sklearn.decomposition import PCA
    X_train, Y_train, X_weights = GetData(training_file, weight=True)
    X_test, Y_test = GetData(test_file)
    X_valid, Y_valid = GetData(valid_file)
    print("performing PCA")
    print("train sample")
    pca = PCA(n_components=44)
    pca.fit(X_train)
    X_train = pca.transform(X_train)
    print("shape train:", np.shape(X_train))
    print("test sample")
    X_test = pca.transform(X_test)
    print("validation sample")
    X_valid = pca.transform(X_valid)

    return X_train, Y_train, X_weights, X_test, Y_test, X_valid, Y_valid


def create_model(X_train, Y_train, X_weights, X_test, Y_test, X_valid, Y_valid):
    """Keras model."""

    space = {'activation_1': 1, 'activation_3': 0, 'activation_2': 0,
                    'activation_5': 1, 'activation_4': 1, 'activation_7': 1,
                    'activation_6': 0, 'activation': 0, 'add': 0,
                    'Dropout_1': 0.2855447016706857,
                    'Dropout_3': 0.2926039999512635,
                    'Dropout_2': 0.721896372375597,
                    'Dropout_5': 0.8232978920685093,
                    'Adam': 0.020312819138588394,
                    'Dropout_7': 0.10466253116015645,
                    'Dropout_6': 0.7585898935293622,
                    'Dropout': 0.3713456613713345,
                    'Dropout_4': 0.7792982875097282}
    space = {'activation_1': 0, 'activation_3': 0, 'activation_2': 0,
                    'activation_5': 0, 'activation_4': 0, 'activation_7': 0,
                    'activation_6': 0, 'activation': 0, 'add': 0,
                    'Dropout_1': 0.2,
                    'Dropout_3': 0.2,
                    'Dropout_2': 0.2,
                    'Dropout_5': 0.2,
                    'Adam': 0.00005,
                    # 'Adam': 0.005,
                    'Dropout_7': 0.2,
                    'Dropout_6': 0.2,
                    'Dropout': 0.2,
                    'Dropout_4': 0.2}
    model = Sequential()
     # -------------------------- 1st layer ------------------------- #

    model.add(Dense(units=78, input_shape=(44,),
                  kernel_initializer='glorot_uniform',
                  activation=['relu', 'sigmoid'][space['activation']]
                 ))
    #model.add(Activation('linear'))
    # model.add([Dropout(0.1), Activation('linear')][space['add']])
    # model.add(Dropout(space['Dropout']))
    model.add(BatchNormalization())
    model.add(Dense(units=64,
                    activation='relu',
                    kernel_initializer='glorot_uniform',
                    ))
    # model.add(Dropout(0.2))
    model.add(BatchNormalization())
    # -------------------------- 2nd layer ------------------------- #
    model.add(Dense(units=57,
                 activation=['relu', 'sigmoid'][space['activation_1']],
                 kernel_initializer='glorot_uniform',
                 ))
    # model.add(Dropout(space['Dropout_1']))
    model.add(BatchNormalization())
    # -------------------------- 3rd layer ------------------------- #
    model.add(Dense(units=60,
                 activation=['relu', 'sigmoid'][space['activation_2']],
                 kernel_initializer='glorot_uniform',
                 ))
    # model.add(Dropout(space['Dropout_2']))
    model.add(BatchNormalization())
    # -------------------------- 4th layer ------------------------- #
    model.add(Dense(units=48,
                 activation=['relu', 'sigmoid'][space['activation_3']],
                 kernel_initializer='glorot_uniform',
                 ))
    # model.add(Dropout(space['Dropout_3']))
    model.add(BatchNormalization())
    model.add(Dense(units=36,
                 activation=['relu', 'sigmoid'][space['activation_4']],
                 kernel_initializer='glorot_uniform',
                 ))
    # model.add(Dropout(space['Dropout_4']))
    model.add(BatchNormalization())
    # -------------------------- 6th layer ------------------------- #
    model.add(Dense(units=24,
                       kernel_initializer='glorot_uniform',
                       activation=['relu', 'sigmoid'][space['activation_5']]
                       # nb_feature=25
                       ))
    # model.add(Dropout(space['Dropout_5']))
    model.add(BatchNormalization())
    # -------------------------- 7th layer ------------------------- #
    model.add(Dense(units=12,
                 activation=['relu', 'sigmoid'][space['activation_6']],
                 kernel_initializer='glorot_uniform',
                 ))
    # model.add(Dropout(space['Dropout_6']))
    model.add(BatchNormalization())
    # -------------------------- 8th layer ------------------------- #
    model.add(Dense(units=6,
                 activation=['relu', 'sigmoid'][space['activation_7']],
                 kernel_initializer='glorot_uniform',
                 ))
    # model.add(Dropout(space['Dropout_7']))
    model.add(BatchNormalization())
    # -------------------------- 9th layer ------------------------- #
    model.add(Dense(3,
           activation="softmax", kernel_initializer='glorot_uniform'))
    # model.summary()
    model_optimizer = Adam(space['Adam'])

    model.compile(  # loss='mse',
        loss='categorical_crossentropy',
        optimizer=model_optimizer,
        metrics=['accuracy'])
    model.summary()
    for epoch_i in range(1, 1000):
        print("start training epoch %i/1000" % epoch_i)
        hist_mod = model.fit(X_train, Y_train,
                             sample_weight=X_weights,
                             batch_size=300, epochs=1,
                             verbose=2,
                             validation_data=(X_valid, Y_valid)
                             )
        model.save('results/%s/model_epoch%i.h5' % ('hyperoptimised_model', epoch_i))
        score, acc = model.evaluate(X_test, Y_test, verbose=0)
        print('Test accuracy:', acc)

if __name__ == '__main__':

    input_samples = data()
    create_model(*input_samples)

# for epoch_i in range(1, 501):
#     print("start training epoch %i" % epoch_i)
    # model.save('results/%s/model_epoch%i.h5' % (args.model_name, epoch_i))
    # loss_list = hist_mod.history['loss']
    # acc_list = hist_mod.history['acc']
    # eval_loss = model.evaluate(X_train, Y_train, batch_size=300,
    #                            verbose=0)
    #
    # loss_file = "results/%s/LossFile_Epoch%i.h5" % (args.model_name,
    #                                                 epoch_i)
    # h5f = h5py.File(loss_file, 'w')
    # h5f.create_dataset('train_loss_list', data=loss_list)
    # h5f.create_dataset('eval_loss_list', data=eval_loss)
    # h5f.create_dataset('train_acc_list', data=acc_list)
    # h5f.close()

# model.save('results/%s/Final_model.h5' % args.model_name)
